package main

// Validate Array
func validate_array(adn []string) (bool, string) {

	if valid, err := validate_array_have_data(adn); !valid {
		return false, err
	}

	if valid, err := validate_array_string_size(adn); !valid {
		return false, err
	}

	if valid, err := validate_array_strings_letters(adn); !valid {
		return false, err
	}

	if valid, err := validate_array_letters(adn); !valid {
		return false, err
	}

	return true, ""
}

// Validate Array
// 1. Array have data
func validate_array_have_data(adn []string) (bool, string) {
	if adn == nil {
		return false, "Array is empty"
	}

	return true, ""
}

// Validate Array
// 2. Array Strings have same size
func validate_array_string_size(adn []string) (bool, string) {
	valid := true
	string_size := len(adn[0])

	for i := 1; i < len(adn); i++ {
		if string_size != len(adn[i]) {
			valid = false
			break
		}
	}

	if !valid {
		return false, "Array Strings not have same size"
	}

	return true, ""
}

// Validate Array
// 3. Number of strings and the number of
// letters per string are the same
func validate_array_strings_letters(adn []string) (bool, string) {
	valid := true
	array_size := len(adn)

	for i := 0; i < len(adn); i++ {

		if array_size != len(adn[i]) {
			valid = false
			break
		}
	}

	if !valid {
		return false, "Number of strings and the number of letters per string are distinct"
	}

	return true, ""
}

// Validate Array
// 4. Strings have valid letter
func validate_array_letters(adn []string) (bool, string) {
	valid := true
	array_valid_letters := [4]string{"A", "T", "G", "C"}

	for i := 0; i < len(adn); i++ {

		for j := 0; j < len(adn[0]); j++ {

			letter := string(adn[i][j])
			valid_letter := false

			for k := 0; k < len(array_valid_letters); k++ {

				if letter == array_valid_letters[k] {
					valid_letter = true
					break
				}
			}

			if !valid_letter {
				valid = false
				break
			}
		}
	}

	if !valid {
		return false, "Strings Have Invalid letters "
	}

	return true, ""
}
