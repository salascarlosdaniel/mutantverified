package main

import "fmt"

var dna_array []string
var dna_matrix [][]string
var dna_matrix_row int
var dna_matrix_col int
var secuencies_mutant_count int

// Verify Mutant dna
func IsMutant(dna []string) (bool, bool) {

	dna_array = dna

	secuencies_mutant_count = 0

	// validate array dna
	success, err := validate_array(dna_array)

	if success {
		// transfrom adn to matrix
		dna_matrix = array_to_matrix(dna_array)
		//fmt.Println(dna_matrix)

		// iterate matrix to search secuencies
		for i := 0; i < dna_matrix_row; i++ {
			for j := 0; j < dna_matrix_col; j++ {
				//fmt.Printf("x = %d , y = %d ,Letra = %s ", i, j, string(dna_matrix[i][j]) )
				secuencies_mutant_count += search_secuencies(i, j)
			}
		}

		if secuencies_mutant_count > 1 {
			fmt.Println(secuencies_mutant_count)
			return true, true
		} else {
			return true, false
		}

	} else {
		fmt.Println(err)
		return false, false
	}
}

// Transform Array To Matrix
func array_to_matrix(array []string) [][]string {

	// dna_matrix_row = dna_matrix_col
	dna_matrix_row = len(array)
	dna_matrix_col = len(array[0])

	// Create Matrix
	matrix := make([][]string, dna_matrix_col)
	for i := range matrix {
		matrix[i] = make([]string, dna_matrix_row)
	}

	// Fill Matrix
	for i := 0; i < dna_matrix_row; i++ {
		for j := 0; j < dna_matrix_col; j++ {
			matrix[i][j] = string(array[i][j])
		}
	}

	return matrix
}
