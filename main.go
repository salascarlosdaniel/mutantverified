package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type Secuency struct {
	Dna []string `json:"dna"`
}

type Stat struct {
	Count_Mutant_Dna int     `json:"count_mutant_dna"`
	Count_Human_Dna  int     `json:"count_human_dna"`
	Ratio            float32 `json:"ratio"`
}

// PostMutantDetectHandler - POST - /mutant/
func PostMutantDetectHandler(w http.ResponseWriter, r *http.Request) {
	secuency := Secuency{}
	err := json.NewDecoder(r.Body).Decode(&secuency)

	if err != nil {
		panic(err)
	}

	// val == true: correct secuency format
	// ismutant == true mutant secuency
	val, ismutant := IsMutant(secuency.Dna)

	if val {
		CreateSecuency(secuency.Dna, ismutant)
		if ismutant {
			// mutant secuency
			w.WriteHeader(http.StatusOK)
		} else {
			// not mutant secuency
			w.WriteHeader(http.StatusForbidden)
		}
	} else {
		// invalid dna forma
		w.WriteHeader(http.StatusNoContent)
	}
}

func GetMutantStatsHandler(w http.ResponseWriter, r *http.Request) {

	var stat Stat

	// Get Struct
	stat, err := ReadSecuencyStats()
	if err != nil {
		panic(err)
	}

	// Struct to Json
	j, err := json.Marshal(stat)
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(j)

}

func main() {

	r := mux.NewRouter().StrictSlash(false)

	r.HandleFunc("/mutant/", PostMutantDetectHandler).Methods("POST")
	r.HandleFunc("/stats/", GetMutantStatsHandler).Methods("GET")

	// Server values
	server := &http.Server{
		Addr:           ":8080",
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := server.ListenAndServe(); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
