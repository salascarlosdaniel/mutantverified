package main

// Search Secuency
// Search dna mutant secuency
func search_secuencies(x int, y int) int {
	var count int

	count += search_horizontal_secuency(x, y)
	count += search_vertical_secuency(x, y)
	count += search_oblique_right_secuency(x, y)
	count += search_oblique_left_secuency(x, y)

	return count
}

// Search Secuency
// 1. Search horizontal dna mutant
func search_horizontal_secuency(x int, y int) int {

	letter := string(dna_matrix[x][y])
	secuency_length := 1

	if y > 0 {
		// evaluate current and previus letter
		if string(dna_matrix[x][y-1]) == letter {
			// the sequence has already been found
			return 0
		}
	}

	for i := y + 1; i < dna_matrix_col; i++ {

		if letter == string(dna_matrix[x][i]) {
			secuency_length++
		} else {
			break
		}

	}

	if secuency_length >= 4 {
		return 1
	}

	return 0
}

// Search Secuency
// 2. Search vertical dna mutant
func search_vertical_secuency(x int, y int) int {

	letter := string(dna_matrix[x][y])
	secuency_length := 1

	if x > 0 {
		// evaluate current and previus letter
		if string(dna_matrix[x-1][y]) == letter {
			// the sequence has already been found
			return 0
		}
	}

	for i := x + 1; i < dna_matrix_row; i++ {

		if letter == string(dna_matrix[i][y]) {
			secuency_length++
		} else {
			break
		}
	}

	if secuency_length >= 4 {
		return 1
	}

	return 0
}

// Search Secuency
// 3. Search oblique right dna mutant
func search_oblique_right_secuency(x int, y int) int {

	letter := string(dna_matrix[x][y])
	secuency_length := 1

	if x > 0 && y > 0 {
		// evaluate current and previus letter
		if string(dna_matrix[x-1][y-1]) == letter {
			// the sequence has already been found
			return 0
		}
	}

	j := y + 1

	for i := x + 1; i < dna_matrix_row; i++ {

		if j < dna_matrix_col {

			if letter == string(dna_matrix[i][j]) {
				secuency_length++
			} else {
				break
			}

		}

		j++
	}

	if secuency_length >= 4 {
		return 1
	}

	return 0
}

// Search Secuency
// 4. Search oblique right dna mutant
func search_oblique_left_secuency(x int, y int) int {

	letter := string(dna_matrix[x][y])
	secuency_length := 1

	if x > 0 && y > dna_matrix_col {
		// evaluate current and previus letter
		if string(dna_matrix[x-1][y+1]) == letter {
			// the sequence has already been found
			return 0
		}
	}

	j := y - 1

	for i := x + 1; i < dna_matrix_row; i++ {

		if j >= 0 && j < dna_matrix_col {

			if letter == string(dna_matrix[i][j]) {
				secuency_length++
			} else {
				break
			}
		}

		j--
	}

	if secuency_length >= 4 {
		return 1
	}

	return 0
}
