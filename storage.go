package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/denisenkom/go-mssqldb"
)

// var (
// 	server   = "DESKTOP-2K2UFIJ"
// 	port     = 1433
// 	user     = "charles"
// 	password = "204080"
// 	database = "Mutants"
// )

var (
	server   = "mutantverifiedproject:southamerica-east1:mutants"
	port     = 1433
	user     = "sqlserver"
	password = "204080"
	database = "Mutants"
)

// CreateEmployee create an employee
func CreateSecuency(dna []string, isMutant bool) (int64, error) {

	conn := connect()
	var ismutant int

	if isMutant {
		ismutant = 1
	} else {
		ismutant = 0
	}

	tsql := fmt.Sprintf("INSERT INTO dbo.Secuencies (dna, isMutant) VALUES ('%s', %d);", dna, ismutant)
	fmt.Println(tsql)
	result, err := conn.Exec(tsql)
	if err != nil {
		fmt.Println("Error inserting new row: " + err.Error())
		return -1, err
	}

	// defer conn.Close()

	return result.LastInsertId()

}

// ReadEmployees read all employees
func ReadSecuencyStats() (Stat, error) {
	var stat Stat

	conn := connect()

	// Execute Query
	tsql := fmt.Sprintf("SELECT isMutant, COUNT(Id) AS quantity FROM dbo.Secuencies GROUP BY isMutant;")
	rows, err := conn.Query(tsql)
	if err != nil {
		fmt.Println("Error reading rows: " + err.Error())
		return stat, err
	}
	defer rows.Close()

	// Rows to Struct
	for rows.Next() {

		var ismutant bool
		var quantity int

		err := rows.Scan(&ismutant, &quantity)
		if err != nil {
			fmt.Println("Error reading rows: " + err.Error())
			return stat, err
		}

		if ismutant {
			stat.Count_Mutant_Dna = quantity
		} else {
			stat.Count_Human_Dna = quantity
		}
	}

	stat.Ratio = float32(stat.Count_Mutant_Dna) / float32(stat.Count_Mutant_Dna+stat.Count_Human_Dna)

	return stat, nil
}

// Connect to database
func connect() *sql.DB {
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;",
		server, user, password, port, database)
	conn, err := sql.Open("mssql", connString)
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}

	fmt.Printf("Connected!\n")

	return conn

}
